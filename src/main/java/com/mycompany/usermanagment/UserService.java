/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagment;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class UserService {
    private static  ArrayList<User> userList = new ArrayList<>();
    
    
    static {
        userList.add(new User("admin","password"));
        userList.add(new User("user1","password"));
    }
    
    public  static boolean  addUser(User user){
        userList.add(user);
        return true;
    }
    
    public  static boolean  addUser(String userName,String password){
        userList.add(new User(userName,password));
        return true;
    }
    
    public  static boolean updateuser(int index,User user){
        userList.set(index, user);
        return true;
    }
    
    public  static User getUser(int  index){
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    
    
    public static  ArrayList<User> getUsers(){
        return  userList;
    }
    public static  ArrayList<User> scarcUserName(String scarchText){
        ArrayList<User> list = new ArrayList<>();
        for(User user: userList){
            if(user.getUserName().startsWith(scarchText)){
                list.add(user);
            }
        }
        return list;
    }
    
    public  static boolean  delUser(int index){
        userList.remove(index);
        return true;
    }
    
    public  static boolean  delUser(User user){
        userList.remove(user);
        return true;
    }
    
    public  static User login(String userName,String password){
        for(User user: userList){
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
        return  null;
    }
    
    public static void save(){
        
    }
    
    public static void load(){
        
    }
}



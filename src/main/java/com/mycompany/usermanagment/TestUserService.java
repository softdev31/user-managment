/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagment;

/**
 *
 * @author ASUS
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService.addUser("user2", "passwprd");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "passwprd"));
        System.out.println(UserService.getUsers());
        
        
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateuser(3, user);
        System.out.println(UserService.getUsers());
        UserService.delUser(3);
        System.out.println(UserService.getUsers());
        
        
        System.out.println(UserService.login("admin", "password"));
    }
}
